FROM debian:9 as build

ENV VER_NGINX_DEVEL_KIT=0.3.0
ENV VER_LUA_NGINX_MODULE=0.10.8
ENV VER_NGINX=1.11.0
ENV VER_LUAJIT=2.1-20220111
ENV VER_LUA_RESTY_CORE=0.1.22
ENV VER_LUA_RESTY_LRUCACHE=0.10

ENV NGINX_DEVEL_KIT ngx_devel_kit-${VER_NGINX_DEVEL_KIT}
ENV LUA_NGINX_MODULE lua-nginx-module-${VER_LUA_NGINX_MODULE}
ENV LUAJIT luajit2-${VER_LUAJIT}
ENV NGINX nginx-${VER_NGINX}
ENV LUA_RESTY_CORE lua-resty-core-${VER_LUA_RESTY_CORE}
ENV LUA_RESTY_LRUCACHE lua-resty-lrucache-${VER_LUA_RESTY_LRUCACHE}

ENV LUAJIT_LIB /usr/local/lib
ENV LUAJIT_INC /usr/local/include/luajit-2.1

WORKDIR /home/nginx_build

# Update and install required packages

RUN apt update && apt install -y wget unzip gcc make libpcre3 libpcre3-dev zlib1g-dev procps

# Download

RUN wget -O ${LUAJIT}.tar.gz http://github.com/openresty/luajit2/archive/refs/tags/v${VER_LUAJIT}.tar.gz
RUN wget -O ${NGINX_DEVEL_KIT}.tar.gz http://github.com/vision5/ngx_devel_kit/archive/refs/tags/v${VER_NGINX_DEVEL_KIT}.tar.gz
RUN wget -O ${LUA_NGINX_MODULE}.tar.gz http://github.com/openresty/lua-nginx-module/archive/v${VER_LUA_NGINX_MODULE}.tar.gz
RUN wget -O ${LUA_RESTY_CORE}.tar.gz http://github.com/openresty/lua-resty-core/archive/refs/tags/v${VER_LUA_RESTY_CORE}.tar.gz
RUN wget -O ${LUA_RESTY_LRUCACHE}.tar.gz http://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v${VER_LUA_RESTY_LRUCACHE}.tar.gz
RUN wget http://nginx.org/download/nginx-${VER_NGINX}.tar.gz

# Extract

RUN tar xvfx ${LUAJIT}.tar.gz && rm ${LUAJIT}.tar.gz
RUN tar xvfx ${NGINX_DEVEL_KIT}.tar.gz && rm ${NGINX_DEVEL_KIT}.tar.gz
RUN tar xvfx ${LUA_NGINX_MODULE}.tar.gz && rm ${LUA_NGINX_MODULE}.tar.gz
RUN tar xvfx ${LUA_RESTY_CORE}.tar.gz && rm ${LUA_RESTY_CORE}.tar.gz
RUN tar xvfx ${LUA_RESTY_LRUCACHE}.tar.gz && rm ${LUA_RESTY_LRUCACHE}.tar.gz
RUN tar xvfx ${NGINX}.tar.gz && rm ${NGINX}.tar.gz

# BUILD FROM SOURCE
# LuaJIT

RUN cd ${LUAJIT} && make install

# Lua resty core with prefix in nginx directory

RUN cd ${LUA_RESTY_CORE} && make install PREFIX=/usr/local/nginx

# Lua resty lrucache with prefix in nginx directory

RUN cd ${LUA_RESTY_LRUCACHE} && make install PREFIX=/usr/local/nginx

# Nginx with JuaJIT

RUN cd ${NGINX} && export LUAJIT_LIB=${LUAJIT_LIB} && export LUAJIT_INC=${LUAJIT_INC} && ./configure \
	--with-ld-opt="-Wl,-rpath,${LUAJIT_LIB}" \
        --add-dynamic-module=/home/nginx_build/${LUA_NGINX_MODULE} \
        --add-dynamic-module=/home/nginx_build/${NGINX_DEVEL_KIT} \
	&& make && make install 



FROM debian:9

WORKDIR /usr/local/nginx/sbin

# Copying nginx binary 

COPY --from=build /usr/local/nginx/sbin/nginx .

# Create nginx folder

RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx

# Copying mime, libs and modules from build

COPY --from=build /usr/local/nginx/conf/mime.types ../conf/
COPY --from=build /usr/local/nginx/modules/ ../modules/
COPY --from=build /usr/local/nginx/lib/ ../lib/
COPY --from=build /usr/local/lib/ ../../lib/
CMD ["./nginx", "-g", "daemon off;"]
