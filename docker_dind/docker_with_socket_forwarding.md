```
[admin@learning less4]$ docker run -ti -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd)/Dockerfile:/home/less4/Dockerfile --name some_docker docker:19.03
/ # docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
docker              dind                c05a8c965912        3 days ago          235MB
docker              latest              6816f070b239        3 days ago          218MB
<none>              <none>              67284726be71        4 days ago          347MB
debian              9                   4e9a8c105fd4        13 days ago         101MB
docker              19.03               33d3743ceb6a        11 months ago       220MB
/ # cd /home/less4/
/home/less4 # docker build -t nginx:lua19 .
Sending build context to Docker daemon   5.12kB
Step 1/48 : FROM debian:9 as build
 ---> 4e9a8c105fd4
Step 2/48 : ENV VER_NGINX_DEVEL_KIT=0.3.0
 ---> Using cache
 ---> 8e311faf83f4
Step 3/48 : ENV VER_LUA_NGINX_MODULE=0.10.8
 ---> Using cache
 ---> f92ff4963007
Step 4/48 : ENV VER_NGINX=1.11.0
 ---> Using cache
 ---> d01ce91ae6f4
Step 5/48 : ENV VER_LUAJIT=2.1-20220111
 ---> Using cache
 ---> 37ae9f8792d1
Step 6/48 : ENV VER_LUA_RESTY_CORE=0.1.22
 ---> Using cache
 ---> 8936693174d4
Step 7/48 : ENV VER_LUA_RESTY_LRUCACHE=0.10
 ---> Using cache
 ---> 128f44c92fcd
Step 8/48 : ENV NGINX_DEVEL_KIT ngx_devel_kit-${VER_NGINX_DEVEL_KIT}
 ---> Using cache
 ---> 914674a75485
Step 9/48 : ENV LUA_NGINX_MODULE lua-nginx-module-${VER_LUA_NGINX_MODULE}
 ---> Using cache
 ---> 5cfd13a20e70
Step 10/48 : ENV LUAJIT luajit2-${VER_LUAJIT}
 ---> Using cache
 ---> ac3262c115f6
Step 11/48 : ENV NGINX nginx-${VER_NGINX}
 ---> Using cache
 ---> c2fa22969134
Step 12/48 : ENV LUA_RESTY_CORE lua-resty-core-${VER_LUA_RESTY_CORE}
 ---> Using cache
 ---> 32fca337b557
Step 13/48 : ENV LUA_RESTY_LRUCACHE lua-resty-lrucache-${VER_LUA_RESTY_LRUCACHE}
 ---> Using cache
 ---> 3761e5e12285
Step 14/48 : ENV LUAJIT_LIB LUAJIT_LIB=/usr/local/lib
 ---> Using cache
 ---> f67779d8f25c
Step 15/48 : ENV LUAJIT_INC LUAJIT_INC=/usr/local/include/luajit-2.1
 ---> Using cache
 ---> 7f6521e8e0bd
...
...
...
...
Step 41/48 : WORKDIR /usr/local/nginx/sbin
 ---> Running in 7fb3888da842
Removing intermediate container 7fb3888da842
 ---> eaaa4558c9c9
Step 42/48 : COPY --from=build /usr/local/nginx/sbin/nginx .
 ---> 508d8e3ff229
Step 43/48 : RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
 ---> Running in a30bcc8de1ee
Removing intermediate container a30bcc8de1ee
 ---> 73754434ca25
Step 44/48 : COPY --from=build /usr/local/nginx/conf/mime.types ../conf/
 ---> 50b40ee9fef5
Step 45/48 : COPY --from=build /usr/local/nginx/modules/ ../modules/
 ---> 89d855ab024d
Step 46/48 : COPY --from=build /usr/local/nginx/lib/ ../lib/
 ---> ec90c29d7bc4
Step 47/48 : COPY --from=build /usr/local/lib/ ../../lib/
 ---> 933ce29a589e
Step 48/48 : CMD ["./nginx", "-g", "daemon off;"]
 ---> Running in a91f1447318a
Removing intermediate container a91f1447318a
 ---> f106981c3726
Successfully built f106981c3726
Successfully tagged nginx:lua19
/home/less4 # docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
nginx               lua19               f106981c3726        2 minutes ago       122MB
docker              dind                c05a8c965912        3 days ago          235MB
docker              latest              6816f070b239        3 days ago          218MB
<none>              <none>              67284726be71        4 days ago          347MB
debian              9                   4e9a8c105fd4        13 days ago         101MB
docker              19.03               33d3743ceb6a        11 months ago       220MB
/home/less4 # exit
[admin@learning less4]$
```
