```
[admin@learning less4]$ docker run --privileged -v $(pwd)/Dockerfile:/home/less4/Dockerfile --name=dockerdind -d docker:dind
6250bd7ff49f71d132a5cd52a8d483b1cf64a20ae6ee17678eb0c6fcf33d58e1
[admin@learning less4]$ docker exec -ti dockerdind sh
/ # cd /home/less4/
/home/less4 # docker build -t nginx:luav10 .
Sending build context to Docker daemon   5.12kB
Step 1/48 : FROM debian:9 as build
9: Pulling from library/debian
ec52bb9d0b76: Pull complete
Digest: sha256:54f2c31487af733ad08e62af6a77ccddcbc8895857edc54768ba0020991950f9
Status: Downloaded newer image for debian:9
 ---> 4e9a8c105fd4
Step 2/48 : ENV VER_NGINX_DEVEL_KIT=0.3.0
 ---> Running in 2917e7b66b6b
Removing intermediate container 2917e7b66b6b
 ---> 15ebe808e674
Step 3/48 : ENV VER_LUA_NGINX_MODULE=0.10.8
 ---> Running in 2dd920fd663b
Removing intermediate container 2dd920fd663b
 ---> 5fcf717db827
Step 4/48 : ENV VER_NGINX=1.11.0
 ---> Running in 4d83d1f67314
Removing intermediate container 4d83d1f67314
 ---> 6cea674a472f
Step 5/48 : ENV VER_LUAJIT=2.1-20220111
 ---> Running in f65dd55cfffe
Removing intermediate container f65dd55cfffe
 ---> 367f9cf4654b
Step 6/48 : ENV VER_LUA_RESTY_CORE=0.1.22
 ---> Running in bc7d29586034
Removing intermediate container bc7d29586034
 ---> 864b8f1ada83
Step 7/48 : ENV VER_LUA_RESTY_LRUCACHE=0.10
 ---> Running in e7f85e830cbf
Removing intermediate container e7f85e830cbf
 ---> 4706ab77d15e
Step 8/48 : ENV NGINX_DEVEL_KIT ngx_devel_kit-${VER_NGINX_DEVEL_KIT}
 ---> Running in d37c970f708a
Removing intermediate container d37c970f708a
 ---> 61a565aa8027
Step 9/48 : ENV LUA_NGINX_MODULE lua-nginx-module-${VER_LUA_NGINX_MODULE}
...
...
...
...
Step 43/48 : RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
 ---> Running in a44a65b9d586
Removing intermediate container a44a65b9d586
 ---> b66e300350e4
Step 44/48 : COPY --from=build /usr/local/nginx/conf/mime.types ../conf/
 ---> 651da20bc71b
Step 45/48 : COPY --from=build /usr/local/nginx/modules/ ../modules/
 ---> df7c20775ff1
Step 46/48 : COPY --from=build /usr/local/nginx/lib/ ../lib/
 ---> d1be5d00b979
Step 47/48 : COPY --from=build /usr/local/lib/ ../../lib/
 ---> 6d9f9435fb74
Step 48/48 : CMD ["./nginx", "-g", "daemon off;"]
 ---> Running in 612887f7e299
Removing intermediate container 612887f7e299
 ---> c39bf2bb0e53
Successfully built c39bf2bb0e53
Successfully tagged nginx:luav10
/home/less4 # docker images
REPOSITORY   TAG       IMAGE ID       CREATED              SIZE
nginx        luav10    c39bf2bb0e53   About a minute ago   122MB
<none>       <none>    d5e475489eb3   About a minute ago   347MB
debian       9         4e9a8c105fd4   13 days ago          101MB
/home/less4 # exit
[admin@learning less4]$ docker images
REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
docker       dind      c05a8c965912   3 days ago      235MB
docker       latest    6816f070b239   3 days ago      218MB
<none>       <none>    67284726be71   4 days ago      347MB
debian       9         4e9a8c105fd4   13 days ago     101MB
docker       19.03     33d3743ceb6a   11 months ago   220MB
[admin@learning less4]$
```
